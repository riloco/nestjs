import { OrdersService } from './../services/orders.service';
import { ApiTags } from '@nestjs/swagger';
import { Request } from 'express';

import { RolesGuard } from './../../auth/guards/roles.guard';
import { JwtAuthGuard } from './../../auth/guards/jwt-auth.guard';
import { PayloadToken } from '../../auth/models/token.model';
import { Controller, Get, Req, UseGuards } from '@nestjs/common';
import { Roles } from 'src/auth/decorator/roles.decorator';
import { Role } from 'src/auth/models/roles.model';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags('profile')

@Controller('profile')
export class ProfileController {

  constructor(private orderSrv: OrdersService){}

  @Roles(Role.CUSTUMER)
  @Get('my-orders')
  getOrders(@Req() req: Request){
    const user = req.user as PayloadToken;
    console.log('user: ', user);

    return this.orderSrv.ordersByUser(user.sub);
  }

}
