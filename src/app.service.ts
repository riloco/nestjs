import { Inject, Injectable } from '@nestjs/common';
//import { ConfigService } from '@nestjs/config';
import { ConfigType } from '@nestjs/config';
import { Db } from 'mongodb'

import config from './config';

@Injectable()
export class AppService {
  constructor(
    //@Inject('API_KEY') private apiKey: string,
    //private configSrv: ConfigService
    @Inject('TASKS') private tasks: any[],
    @Inject('MONGO') private database: Db,
    @Inject(config.KEY) private configSrv: ConfigType<typeof config>,
  ) {}

  getHello(): string {
    // console.log(this.tasks);
    // const apikey = this.configSrv.get<string>('API_KEY');
    const apikey = this.configSrv.apikey;
    const bd_name = this.configSrv.database.name;

    return `Hello World! ${apikey} ${bd_name}`;
  }

  getTasks(){
    const tasksCollections = this.database.collection('task');
    return tasksCollections.find().toArray();
  }
}
