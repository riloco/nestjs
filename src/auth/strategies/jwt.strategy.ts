import { Injectable, Inject } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";

import { ExtractJwt, Strategy } from "passport-jwt";
import { ConfigType } from '@nestjs/config';

import { PayloadToken } from './../models/token.model';
import config from '../../config'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {

  constructor(@Inject(config.KEY) configSrv: ConfigType<typeof config>) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configSrv.jwtSecret
    })
  }

  validate(payload: PayloadToken){
    // console.log('pyl: ', payload);

    return payload;
  }
}
