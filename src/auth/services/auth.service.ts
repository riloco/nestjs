import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt'
import { JwtService } from '@nestjs/jwt';

import { PayloadToken } from './../models/token.model';
import { User } from './../../users/entities/user.entity';
import { UsersService } from './../../users/services/users.service';

@Injectable()
export class AuthService {

  constructor( private userSrv: UsersService, private jwtSrv: JwtService){}

  async validateUser(email: string, password: string){

    const user = await this.userSrv.findByEmail(email);

    if (user ) {
      const isMatch = await bcrypt.compare(password, user.password);
      if (isMatch) {
        const {password, ...rta} = user.toJSON();
        return rta
      }
    }
    return null;
  }

  generarJWT(user: User){
    const payload: PayloadToken = { role: user.role, sub: user._id};
    return{
      access_token: this.jwtSrv.sign(payload),
      user,
    }
  }

}
