import { FilterProductsDto } from './../dtos/products.dto';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Mongoose } from 'mongoose';

import { createProductDto, updateProductDto } from '../dtos/products.dto';
import { Product } from '../entities/product.entity';

@Injectable()
export class ProductsService {

  constructor(@InjectModel(Product.name) private productModel: Model<Product>) { }

  findAll(params?: FilterProductsDto) {
    console.log('COnsulta con filtro');
    if (params) {
      const filters: FilterQuery<Product> = {}; // 👈 create filters
      const { limit, offset } = params;
      const { minPrice, maxPrice } = params; // 👈
      console.log('min:', minPrice, 'max:', maxPrice);
      if (minPrice && maxPrice) {
        console.log('min:', minPrice, 'max:', maxPrice);
        filters.price = { $gte: minPrice, $lte: maxPrice };
      }
      return this.productModel.find(filters).populate('brand').skip(offset).limit(limit).exec();
    }
    return this.productModel.find().populate('brand').exec();//
  }

  async findOne(id: string) {
    const producto = await this.productModel.findById(id).exec();
    if (!producto) {
      // throw 'random error';
      throw new NotFoundException(`Product #${id} not found`);
    }
    return producto;
  }

  create(data: createProductDto) {
    const newProduct = new this.productModel(data);
    return newProduct.save();
  }

  delete(id: string) {
    console.log('id:', id);

    return this.productModel.findByIdAndRemove(id);
  }

  update(id: string, changes: updateProductDto) {
    const product = this.productModel.findByIdAndUpdate(id, { $set: changes }, { new: true }).exec();
    if (!product) {
      throw new NotFoundException(`Product #${id} not found`);
    }
    return product;
  }

}
