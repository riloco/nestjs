import {
  Controller,
  Param,
  Get,
  Query,
  Post,
  Body,
  Delete,
  Put,
  HttpCode,
  HttpStatus,
  UseGuards
} from '@nestjs/common'; //ParseIntPipe
import { ApiTags, ApiOperation } from '@nestjs/swagger';

import { ProductsService } from '../services/products.service';
import { ParseIntPipe } from '../../common/parse-int.pipe'; //importamos nuestro propio pipe
import { JwtAuthGuard } from './../../auth/guards/jwt-auth.guard';
import { createProductDto, updateProductDto } from '../dtos/products.dto';
import { MongoIdPipe } from './../../common/mongo-id.pipe';
import { FilterProductsDto } from './../dtos/products.dto';
import { Public } from 'src/auth/decorator/public.decorator';
import { Role } from 'src/auth/models/roles.model';
import { Roles } from 'src/auth/decorator/roles.decorator';

@UseGuards(JwtAuthGuard)
@ApiTags('products')
@Controller('products')
export class ProductsController {
  constructor(private productSrv: ProductsService) {}

  @Public()
  @Get()
  @ApiOperation({ summary: 'List of produts' })
  getProducts(@Query() params: FilterProductsDto) {
    // return `Products: limits => ${limit} offset => ${offset} brand => ${brand}`;
    return this.productSrv.findAll(params);
  }

  @Public()
  @Get('filter')
  getProductFilter(): string {
    return `Yo soy un filter`;
  }

  @Public()
  @Get(':productId')
  @HttpCode(HttpStatus.ACCEPTED)
  getOne(@Param('productId', MongoIdPipe) productId: string): any {
    // return `Product ${productId}`;
    return this.productSrv.findOne(productId);
  }

  @Roles(Role.ADMIN)
  @Post()
  create(@Body() payload: createProductDto) {
    return this.productSrv.create(payload);
  }

  @Put(':id')
  update(@Param('id', MongoIdPipe) id: string, @Body() payload: updateProductDto) {
    return this.productSrv.update(id, payload);
  }

  @Delete(':id')
  delete(@Param('id', MongoIdPipe) id: string) {
    return this.productSrv.delete(id);
  }

}
