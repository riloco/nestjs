import { Controller, Get, Param, Query, UseGuards, SetMetadata } from '@nestjs/common';
import { AppService } from './app.service';

import { ApikeyGuard } from './auth/guards/apikey.guard'
import { Public } from './auth/decorator/public.decorator'

@UseGuards(ApikeyGuard)
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  // @SetMetadata('isPublic', true)
  @Public()
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/ruta/')
  hello(): string {
    return `con /ruta/`;
  }

  @Get('/tasks/')
  tasks(){
    return this.appService.getTasks();
  }

}
