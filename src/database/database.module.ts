import { Global, Module } from '@nestjs/common';
import { MongoClient } from 'mongodb';
import { ConfigType } from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose'

import config from '../config'

const API_KEY = '12345634';
const API_KEY_PROD = 'PROD1212121SA';

  // const taskCollection = database.collection('task');
  // const tasks = await taskCollection.find().toArray();
  // console.log('coneccion ok', tasks);

@Global() //con este decorador ya no es necesario importar en otros modulos lo que se exporte aqui
@Module({imports: [
  // MongooseModule.forRoot('mongodb://localhost:27017', {
  //   user: 'root',
  //   pass: 'root',
  //   dbName: 'platzi-store'
  // })
  MongooseModule.forRootAsync({
    useFactory: (configSrv: ConfigType<typeof config>) =>{
      const {connection, user, password, host, port, dbName} = configSrv.mongo;
      return{
        uri: `${connection}://${host}:${port}`,
        user,
        pass: password,
        dbName
      };
    }, inject: [config.KEY]
  })
],
  providers: [
    {
      provide: 'API_KEY',
      useValue: process.env.NODE_ENV === 'prod' ? API_KEY_PROD : API_KEY,
    },
    {
      provide: 'MONGO',
      useFactory: async (configSrv: ConfigType<typeof config>) => {
        const {connection, user, password, host, port, dbName} = configSrv.mongo;
        const uri = `${connection}://${user}:${password}@${host}:${port}/?authSource=admin&readPreference=primary`;
        const client = new MongoClient(uri);
        await client.connect();
        const database = client.db('platzi-store');
        return database;
      },
      inject: [config.KEY],
    },
  ],
  exports: ['API_KEY', 'MONGO', MongooseModule], //para que pueda ser usado desde cualquier modulo
})
export class DatabaseModule {}
